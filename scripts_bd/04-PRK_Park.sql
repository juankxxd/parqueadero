CREATE TABLE "PRK_Park"
(
    park_id SERIAL NOT NULL,
    direction CHARACTER VARYING(64) NOT NULL,
    name_park CHARACTER VARYING(64) NOT NULL,

    PRIMARY KEY (park_id)
);