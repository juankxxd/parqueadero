CREATE TABLE "PRK_Vehicle_Type"
(
    vehicle_type_id SMALLSERIAL NOT NULL,
    description_vehicle CHARACTER VARYING(64) NOT NULL,
    cost_vehicle INTEGER NOT NULL,

    PRIMARY KEY (vehicle_type_id)
);

INSERT INTO "PRK_Vehicle_Type"(description_vehicle, cost) VALUES('bicicleta', 1000);
INSERT INTO "PRK_Vehicle_Type"(description_vehicle, cost) VALUES('moto', 1700);
INSERT INTO "PRK_Vehicle_Type"(description_vehicle, cost) VALUES('carro', 3000);