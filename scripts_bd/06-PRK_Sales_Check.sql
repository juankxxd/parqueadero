CREATE TABLE "PRK_Sales_Check"
(
    sales_check_id SERIAL NOT NULL,
    identification CHARACTER VARYING(64) NOT NULL,
    start_date timestamp NOT NULL,
    end_date timestamp,
    plate_identification CHARACTER VARYING(8) NOT NULL,
    status_id INTEGER NOT NULL,
    vehicle_type_id INTEGER NOT NULL,
    total INTEGER NOT NULL,
    park_id INTEGER NOT NULL,
    observation CHARACTER VARYING(256),

    PRIMARY KEY (sales_check_id),

    CONSTRAINT "FK_PRK_USERS_IDENTIFICATION_USER" FOREIGN KEY (identification)
        REFERENCES "PRK_Users" (identification) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,

    CONSTRAINT "FK_PRK_PARK_PARK_ID" FOREIGN KEY (park_id)
        REFERENCES "PRK_Park" (park_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,

    CONSTRAINT "FK_PRK_STATUS_STATUS_ID" FOREIGN KEY (status_id)
        REFERENCES "PRK_Status" (status_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,

    CONSTRAINT "FK_PRK_VEHICLE_VEHICLE_TYPE_ID" FOREIGN KEY (vehicle_type_id)
        REFERENCES "PRK_Vehicle_Type" (vehicle_type_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
);