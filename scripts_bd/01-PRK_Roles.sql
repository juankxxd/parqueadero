CREATE TABLE "PRK_Roles"
(
    role_id SERIAL NOT NULL,
    name_role CHARACTER VARYING(64) NOT NULL,

    PRIMARY KEY (role_id)
);

INSERT INTO "PRK_Roles"(name_role) VALUES('super-admin');
INSERT INTO "PRK_Roles"(name_role) VALUES('admin');
INSERT INTO "PRK_Roles"(name_role) VALUES('vendedor');