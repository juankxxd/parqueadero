CREATE TABLE "PRK_Status"
(
    status_id SMALLSERIAL NOT NULL,
    description_status CHARACTER VARYING(64) NOT NULL,

    PRIMARY KEY (status_id)
);

INSERT INTO "PRK_Status"(description_status) VALUES('parqueado');
INSERT INTO "PRK_Status"(description_status) VALUES('cancelado');